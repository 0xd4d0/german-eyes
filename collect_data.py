#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from shodan import Shodan

api = ""
with open("shodan_token","r") as t:
    api = Shodan(t.read().rstrip())

number_of_webcams = api.count("webcam country:'DE'")

print(number_of_webcams)
